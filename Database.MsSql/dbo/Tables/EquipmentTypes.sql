﻿CREATE TABLE [dbo].[EquipmentTypes] (
    [Id]						BIGINT         IDENTITY (1, 1)	NOT NULL,
    [Name]						NVARCHAR (MAX)					NOT NULL,
	[EquipmentTypeGroupId]		BIGINT							NOT NULL,
	PRIMARY KEY CLUSTERED ([Id] ASC),
	CONSTRAINT [FK_EquipmentTypes_ToEquipmentTypeGroups] FOREIGN KEY (EquipmentTypeGroupId) REFERENCES [dbo].[EquipmentTypeGroups]([Id])
);