﻿CREATE TABLE [dbo].[EquipmentTypeAttributes] (
    [Id]						BIGINT IDENTITY (1, 1)	NOT NULL,
    [Name]						NVARCHAR (MAX)			NOT NULL,
    [Description]				NVARCHAR (MAX)			NULL,
	[EquipmentTypeId]			BIGINT					NOT NULL,
	[UnitId]					BIGINT					NOT NULL DEFAULT(0),
    [MinValue]					DECIMAL					NULL,
    [MaxValue]					DECIMAL					NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC), 
    CONSTRAINT [FK_EquipmentTypeAttributes_ToEquipmentTypes] FOREIGN KEY ([EquipmentTypeId]) REFERENCES [dbo].[EquipmentTypes]([Id]),
	CONSTRAINT [FK_EquipmentTypeAttributes_ToUnits] FOREIGN KEY ([UnitId]) REFERENCES [dbo].[Units]([Id])
);

