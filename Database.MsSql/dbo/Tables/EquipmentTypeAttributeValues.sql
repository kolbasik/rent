﻿CREATE TABLE [dbo].[EquipmentTypeAttributeValues] (
    [Id]						BIGINT IDENTITY (1, 1)	NOT NULL,
	[EquipmentTypeAttributeId]	BIGINT					NOT NULL,
	[ValueString]				NVARCHAR (MAX)			NULL,			
	[ValueNumber]				DECIMAL (18, 1)			NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC), 
    CONSTRAINT [FK_EquipmentTypeAttributeValues_ToEquipmentTypeAttributes] FOREIGN KEY ([EquipmentTypeAttributeId]) REFERENCES [dbo].[EquipmentTypeAttributes]([Id])
);

