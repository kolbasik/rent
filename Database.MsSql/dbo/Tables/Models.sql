﻿CREATE TABLE [dbo].[Models] (
	[Id]				BIGINT IDENTITY (1, 1)	NOT NULL,
	[Name]				NVARCHAR (MAX)			NOT NULL,
	[BrandId]			BIGINT					NOT NULL,
	[EquipmentTypeId]	BIGINT					NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC),
	CONSTRAINT [FK_Models_ToBrands] FOREIGN KEY ([BrandId]) REFERENCES [dbo].[Brands]([Id]),
	CONSTRAINT [FK_Models_ToEquipmentTypes] FOREIGN KEY ([EquipmentTypeId]) REFERENCES [dbo].[EquipmentTypes]([Id])
);
