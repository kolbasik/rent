﻿CREATE TABLE [dbo].[EquipmentAttributes] (
    [Id]						BIGINT IDENTITY(1, 1)	NOT NULL,
    [EquipmentId]				BIGINT					NOT NULL,
	[EquipmentTypeAttributeId]	BIGINT					NOT NULL,
	[ValueString]				NVARCHAR (MAX)			NULL,
	[ValueNumber]				DECIMAL (18, 1)			NULL,
    PRIMARY KEY CLUSTERED ([EquipmentId] ASC),
    CONSTRAINT [FK_EquipmentAttributes_ToEquipments] FOREIGN KEY ([EquipmentId]) REFERENCES [dbo].[Equipments]([Id]),
	CONSTRAINT [FK_EquipmentAttributes_ToEquipmentTypeAttributes] FOREIGN KEY ([EquipmentTypeAttributeId]) REFERENCES [dbo].[EquipmentTypeAttributes]([Id])
);

