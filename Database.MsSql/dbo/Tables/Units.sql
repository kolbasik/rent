﻿CREATE TABLE [dbo].[Units] (
	[Id]			BIGINT IDENTITY(1, 1)	NOT NULL,
	[Name]			NVARCHAR(MAX)			NOT NULL,
	PRIMARY KEY CLUSTERED ([Id] ASC)
);