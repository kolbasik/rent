﻿CREATE TABLE [dbo].[Equipments] (
    [Id]				BIGINT IDENTITY(1, 1)	NOT NULL,
    [Name]				NVARCHAR (MAX)			NOT NULL,
	[BrandId]			BIGINT					NOT NULL,
    [EquipmentTypeId]   BIGINT					NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC), 
    CONSTRAINT [FK_Equipments_ToBrands] FOREIGN KEY ([BrandId]) REFERENCES [dbo].[Brands]([Id]), 
    CONSTRAINT [FK_Equipments_ToEquipmentTypes] FOREIGN KEY ([EquipmentTypeId]) REFERENCES [dbo].[EquipmentTypes]([Id])
);

