﻿using System;
using System.Data.Entity;

namespace Rent.Data
{
	public interface IRentDataContext : IDisposable
	{
		DbSet<Brand> Brands { get; }
		DbSet<EquipmentAttribute> EquipmentAttributes { get; }
		DbSet<Equipment> Equipments { get; }
		DbSet<EquipmentTypeAttribute> EquipmentTypeAttributes { get; }
		DbSet<EquipmentType> EquipmentTypes { get; }
		DbSet<Unit> Units { get; }

		IRentDataContext DisableProxy();

		int SaveChanges();
	}

	public partial class RentDataContext : IRentDataContext
	{
		public IRentDataContext DisableProxy()
		{
			this.Configuration.LazyLoadingEnabled = false;
			this.Configuration.ProxyCreationEnabled = false;

			return this;
		}
	}

	public partial class EquipmentAttribute
	{
		public object Value
		{
			get { return (object) ValueString ?? ValueNumber; }
		}
	}
}