﻿(function (angular, $) {
	'use strict';

	var app = angular.module('ent', []);
	
	$.browser = $.browser || {};
	$.browser.device = (/android|webos|iphone|ipad|ipod|blackberry|iemobile|opera mini/i.test(navigator.userAgent.toLowerCase()));

	if (!$.browser.device) {
		app.directive('entAccountLogin', function() {
			return {
				restrict: 'A',
				link: function($scope, element, attrs) {
					element.on('click', function(event) {
						event.preventDefault();
						alert('open login page');
					});
				}
			};
		});
	}

})(angular, jQuery);