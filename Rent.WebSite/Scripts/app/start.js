﻿(function ($) {
	var device = (/android|webos|iphone|ipad|ipod|blackberry|iemobile|opera mini/i.test(navigator.userAgent.toLowerCase()));
	if (!device) {
		var load = $.getScript;
		load('/bundles/angular').done(function () {
			load('/bundles/app').done(function () {
				angular.bootstrap(document, ['ent']);
			});
		});
	}
})(jQuery);