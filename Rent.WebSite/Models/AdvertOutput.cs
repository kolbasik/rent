﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Rent.WebSite.Models
{
	public class AdvertInput
	{
		[Required]
		public string Name { get; set; }

		[Required]
		public int BrandId { get; set; }

		[Required]
		public int EquipmentTypeId { get; set; }

		[Required]
		public ICollection<AdvertInputAttribute> Attributes { get; set; }
	}

	public class AdvertInputAttribute
	{
		public int Id { get; set; }
		public string Value { get; set; }
	}

	public class AdvertOutput
	{
		[Required]
		public string Name { get; set; }

		[Required]
		public int BrandId { get; set; }

		[Required]
		public int EquipmentTypeId { get; set; }
		public ICollection<SelectListItem> Brands { get; set; }
		public ICollection<SelectListItem> EquipmentTypes { get; set; }
	}
}