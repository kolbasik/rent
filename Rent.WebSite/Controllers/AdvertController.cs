﻿using System;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;
using System.Web.Routing;
using Rent.Data;
using Rent.WebSite.Models;

namespace Rent.WebSite.Controllers
{
	public class AdvertController : AbstractController
	{
		public AdvertController()
			: base(new RentDataContext())
		{
		}

		public ActionResult Details(long id)
		{
			var equipment = DataContext.Equipments.Find(id);
			return View(equipment);
		}

		[HttpGet]
		public ActionResult Create()
		{
			var output = new AdvertOutput();
			output.Brands = DataContext.Brands.ToArray()
			                            .Select(brand => new SelectListItem {Text = brand.Name, Value = brand.Id.ToString(CultureInfo.InvariantCulture)})
			                            .ToList();
			output.EquipmentTypes = DataContext.EquipmentTypes.ToArray()
			                                    .Select(equipmentType => new SelectListItem { Text = equipmentType.Name, Value = equipmentType.Id.ToString(CultureInfo.InvariantCulture) })
			                                    .ToList();
			output.BrandId = output.Brands.Select(x => Convert.ToInt32(x.Value)).LastOrDefault();
			output.EquipmentTypeId = output.EquipmentTypes.Select(x => Convert.ToInt32(x.Value)).LastOrDefault();
			return View(output);
		}

		[HttpPost]
		public ActionResult Create(AdvertInput model)
		{
			ActionResult actionResult;
			if (ModelState.IsValid == false)
			{
				actionResult = RedirectToAction("Fail");
			}
			else
			{
				var equipment = new Equipment { BrandId = model.BrandId, EquipmentTypeId = model.EquipmentTypeId, Name = model.Name };
				DataContext.Equipments.Add(equipment);
				foreach (var attirbute in model.Attributes)
				{
					var equipmentAttribute = new EquipmentAttribute
						{
							EquipmentTypeAttributeId = attirbute.Id,
							ValueString = attirbute.Value
						};
					equipment.EquipmentAttributes.Add(equipmentAttribute);
				}
				DataContext.SaveChanges();
				actionResult = Json(new { redirect = "/Advert/Details/" + equipment.Id });
			}
			return actionResult;
		}

		public JsonResult EquipmentTypeAttributes(long id)
		{
			var attributes = DataContext.DisableProxy().EquipmentTypeAttributes.Where(e => e.EquipmentTypeId == id).ToArray();
			return Json(attributes, JsonRequestBehavior.AllowGet);
		}
	}
}