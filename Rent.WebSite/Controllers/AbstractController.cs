﻿using System;
using System.Web.Mvc;
using Rent.Data;

namespace Rent.WebSite.Controllers
{
	public abstract class AbstractController : Controller
	{
		protected readonly IRentDataContext DataContext;

		protected AbstractController(IRentDataContext dataContext)
		{
			if (dataContext == null) throw new ArgumentNullException("dataContext");

			DataContext = dataContext;
		}

		protected override void Dispose(bool disposing)
		{
			DataContext.Dispose();
			base.Dispose(disposing);
		}
	}
}