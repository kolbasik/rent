﻿using Rent.Data;
using System.Linq;
using System.Web.Mvc;

namespace Rent.WebSite.Controllers
{
	public class HomeController : AbstractController
	{
		public HomeController()
			: base(new RentDataContext())
		{
		}

		public ActionResult Index()
		{
			var equipmentTypes = DataContext.EquipmentTypes.ToArray();
			return View(equipmentTypes);
		}

		public ActionResult Equipments(int id, int page = 0, int count = 10)
		{
			var equipments = DataContext.Equipments.Where(x => x.EquipmentTypeId == id).OrderBy(x => x.Id).Skip(page*count).Take(count).ToArray();
			var equipmentsType = DataContext.EquipmentTypes.FirstOrDefault(x => x.Id == id);
			ViewBag.EquipmentsType = equipmentsType == null ? "В базе нет техники такого типа" : equipmentsType.Name;
			ViewBag.Equipments = equipments;
			return View();
		}

		public ActionResult Contact()
		{
			ViewBag.Message = "Your contact page.";

			return View();
		}
	}
}